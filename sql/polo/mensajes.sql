create table mensajes (
    id varchar(36) not null,
    mensaje varchar(500) not null ,
    usuario varchar(50) not null,
    creado timestamp default current_timestamp,
    primary key(id)
) CHARACTER SET utf8mb4
  COLLATE utf8mb4_bin;

