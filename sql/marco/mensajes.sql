create table mensajes (
    id varchar(36) not null,
    mensaje varchar(500) not null ,
    usuario varchar(50) not null,
    creado timestamp default current_timestamp,
    primary key(id)
) CHARACTER SET utf8mb4
  COLLATE utf8mb4_bin;

insert into mensajes
(id, mensaje, usuario)
values
('8218e5e0-780d-4b48-8e85-2e8f3e4efa42', 'ipsum ac tellus semper interdum mauris ullamcorper purus sit', 'bfuxman0'),
('071bac22-4b5c-43de-a4d3-f57893358ef7', 'semper interdum mauris ullamcorper purus', 'tgrzelczyk1'),
('62caf7bd-beaa-494a-a051-fcd9f3df3090', 'condimentum curabitur in libero ut massa', 'cfossey2'),
('5fd94a9e-5736-4da2-8b90-8555cffed395', 'id luctus nec molestie sed justo', 'bmcgarel3'),
('58c8c480-9900-49f9-b1c7-277feb66f1f1', 'quis orci nullam molestie nibh in lectus pellentesque at', 'tallsepp4'),
('3acbc277-dc0c-49c8-bb1e-c2a03e8d89d1', 'vulputate luctus cum sociis natoque', 'fwinscom5'),
('5e85fa28-aec3-426c-8b99-4cf7ed5b16cf', 'posuere cubilia curae donec pharetra magna', 'mflight6'),
('1d0bd934-1a7d-4cb8-ab3a-2573dbbd147c', 'et tempus semper est quam', 'gkornyshev7'),
('01265868-c899-4a15-977a-37ff9eef619e', 'dignissim vestibulum vestibulum ante ipsum', 'dsliney8'),
('de60a575-678e-4af4-aad7-0f6b236b6d68', 'amet justo morbi ut odio cras mi pede malesuada in', 'qwigglesworth9');

