package com.example.dos.app;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Service
@Log4j2
public class CopiarMensaje {

    private static final String QUERY = "select id, mensaje, usuario from mensajes";
    private static final String INSERT = "insert into mensajes(id, mensaje, usuario) VALUES (?, ?, ?)";

    private final JdbcTemplate origen;
    private final JdbcTemplate destino;

    public CopiarMensaje(@Qualifier("jdbcTemplate1") JdbcTemplate origen,
                         @Qualifier("jdbcTemplate2") JdbcTemplate destino) {
        this.origen = origen;
        this.destino = destino;
    }

    public void copiar() {
        origen.query(QUERY, this::processRow);
    }

    private void insertar(String id, String mensaje, String usuario) {
        log.info("Insertando datos en mensaje: {} {} {}", id, mensaje, usuario);
        destino.update(INSERT, id, mensaje, usuario);
    }

    private void processRow(ResultSet rs) throws SQLException {
        String id = UUID.randomUUID().toString();
        insertar(id, rs.getString("mensaje"), rs.getString("usuario"));
    }
}
