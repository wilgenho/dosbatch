package com.example.dos;

import com.example.dos.app.CopiarMensaje;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class DosApplication {

	public static void main(String[] args) {
		SpringApplication.run(DosApplication.class, args);
	}

}
