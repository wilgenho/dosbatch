package com.example.dos.mensajes.batch;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajeRowMapper implements RowMapper<MensajeDTO> {

    @Override
    public MensajeDTO mapRow(ResultSet rs, int i) throws SQLException {
        MensajeDTO dto = new MensajeDTO();
        dto.setId(rs.getString("id"));
        dto.setMensaje(rs.getString("mensaje"));
        dto.setUsuario(rs.getString("usuario"));
        return dto;
    }
}
