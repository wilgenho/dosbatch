package com.example.dos.mensajes.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class BorrarMensajeBatch {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean(name = "borrarMensajeStep")
    public Step borrarMensajeStep(BorrarMensajesTask borrarMensajesTask) {
        return stepBuilderFactory
                .get("borrarMensajeStep")
                .tasklet(borrarMensajesTask)
                .build();
    }

    @Bean(name = "imprimirParametroStep")
    public Step imprimirParametroStep() {
        return stepBuilderFactory
                .get("imprimirParametroStep")
                .tasklet(imprimirParametroTask(null))
                .build();
    }

    @StepScope
    @Bean(name = "imprimirParametroTask")
    public Tasklet imprimirParametroTask(@Value("#{jobParameters['id']}") String id) {
        return (stepContribution, chunkContext) -> {
            log.info("El parametro id es {}", id);
            return RepeatStatus.FINISHED;
        };
    }

    @Bean(name = "borrarMensajeJob")
    public Job borrarMensajeJob(@Qualifier("borrarMensajeStep") Step borrarMensajeStep,
                                @Qualifier("imprimirParametroStep") Step imprimirParametroStep) {
        return jobBuilderFactory
                .get("borrarMensajes")
                .validator(new MensajeIdParameteterValidator())
                .incrementer(new RunIdIncrementer())
                .start(borrarMensajeStep)
                .next(imprimirParametroStep)
                .build();
    }
}
