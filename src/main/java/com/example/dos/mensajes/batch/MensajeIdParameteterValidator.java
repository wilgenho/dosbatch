package com.example.dos.mensajes.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.JobParametersValidator;
import org.springframework.util.StringUtils;

@Slf4j
public class MensajeIdParameteterValidator implements JobParametersValidator {

    @Override
    public void validate(JobParameters jobParameters) throws JobParametersInvalidException {
        String id = jobParameters.getString("id");

        if (!StringUtils.hasText(id)) {
            throw new JobParametersInvalidException("Parameter id is missing");
        } else {
            log.info("I found id parameter");
        }
    }
}
