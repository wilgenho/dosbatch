package com.example.dos.mensajes.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BorrarMensajesTask implements Tasklet {

    private static final String DELETE_SQL = "delete from mensajes";
    private static final String DELETE_ID_SQL = "delete from mensajes where id = ?";

    private final JdbcTemplate jdbcTemplate;

    public BorrarMensajesTask(@Qualifier("jdbcTemplate2") JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        String id = (String) chunkContext.getStepContext().getJobParameters().get("id");
        if (id != null) {
            log.info("Borro el mensaje con Id {}", id);
            jdbcTemplate.update(DELETE_ID_SQL, id);
        } else {
            log.info("Borro todos los mensajes");
            jdbcTemplate.update(DELETE_SQL);
        }

        return RepeatStatus.FINISHED;
    }





}
