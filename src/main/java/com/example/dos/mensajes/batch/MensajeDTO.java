package com.example.dos.mensajes.batch;


import lombok.Data;

@Data
public class MensajeDTO {

    private String id;
    private String mensaje;
    private String usuario;
}
