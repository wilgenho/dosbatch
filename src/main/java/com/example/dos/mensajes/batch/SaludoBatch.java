package com.example.dos.mensajes.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class SaludoBatch {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean(name = "saludarJob")
    public Job saludarJob() {
        return jobBuilderFactory
                .get("saludarJob")
                .incrementer(new DailyJobTimestamper())
                .start(saludarStep())
                .build();
    }

    @Bean(name = "saludarStep")
    public Step saludarStep() {
        return stepBuilderFactory
                .get("saludarStep")
                .tasklet(saludarTask(null))
                .build();
    }

    @Bean(name = "saludarTask")
    @StepScope
    public Tasklet saludarTask(@Value("#{jobParameters['nombre']}") String nombre) {
        return (stepContribution, chunkContext) -> {
            log.info("Hola {}!", nombre);
            return RepeatStatus.FINISHED;
        };
    }

}
