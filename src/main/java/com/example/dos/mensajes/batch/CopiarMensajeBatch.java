package com.example.dos.mensajes.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class CopiarMensajeBatch {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public JdbcCursorItemReader<MensajeDTO> msgReader(@Qualifier("db1") DataSource dataSource) {
        return  new JdbcCursorItemReaderBuilder<MensajeDTO>()
                .dataSource(dataSource)
                .name("mensajeReader")
                .sql("select id, mensaje, usuario from mensajes")
                .rowMapper(new MensajeRowMapper())
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<MensajeDTO> msgWriter(@Qualifier("db2") DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<MensajeDTO>()
                .dataSource(dataSource)
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("insert into mensajes (id, mensaje, usuario) values (:id, :mensaje, :usuario)")
                .build();
    }

    @Bean(name = "copiarMensajeJob")
    public Job copiarMensajeJob(@Qualifier("copiaMensajeStep1") Step copiaMensajeStep1) {
        return jobBuilderFactory.get("copiarMensaje")
                .incrementer(new RunIdIncrementer())
                .flow(copiaMensajeStep1)
                .end().build();
    }

    @Bean(name = "copiaMensajeStep1")
    public Step copiaMensajeStep1(JdbcCursorItemReader<MensajeDTO> msgReader,
                                  JdbcBatchItemWriter<MensajeDTO> msgWriter) {
        return stepBuilderFactory.get("copiarMensajeStep1")
                .<MensajeDTO, MensajeDTO>chunk(5)
                .reader(msgReader)
                .writer(msgWriter)
                .build();
    }

}
